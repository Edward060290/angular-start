import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "angular-rating";
  /*
   * A question text that will show in the front for the rating
   */
  public question = "What do you rate me from 1 to 5?";

  /*
   * Name of the candidate
   */
  public name = "Eduardo Barrañon";
  /*
   * Array of numbers that will show to rate the candidate
   */
  public numbers = [1, 2, 3, 4, 5];

  /*
   * The counter that will the numbers that were click
   */
  private selectedValued: number = 0;

  /*
   * Array that save the sentence comment
   */
  public newPost = [];

   /*
   * Varibale that recieve the comment on a string
   */
  public enteredValue = "";
 
 /*
   * Array that save tje new array of words
   */
  public answer = [];

  /*
   * Function that counts the number of stars click
   */
  public countRate(rateNumber) {
    this.selectedValued = rateNumber;
  }

  /*
   * Function that will rmove the colar red when that mouse is off
   */
  public removeColor(rateNumber) {
    let start = "";
    for (let i = rateNumber - 1; i >= this.selectedValued; i--) {
      start = "starId" + i;
      document.getElementById(start).classList.remove("selected");
    }
  }
  /*
   * Function that will add the colar red when that mouse is over
   */
  public addColor(rateNumber) {
    let start = "";
    for (let i = 0; i < rateNumber; i++) {
      start = "starId" + i;
      document.getElementById(start).classList.add("selected");
    }
  }

   /*
   * Function that revices the comment from the front to convert the new array an save
   * tha first and last element of a string and tha count in the middel
   */
  onAddPost() {
    let firstElement = "";
    let lastElement = "";
    let index = 0;
    let countMiddle: number;
    this.newPost = this.enteredValue.split(" ");
     this.newPost.forEach((x,i)=>{
          // we obtain the first element string of the string word
          firstElement = this.newPost[i][0]
           // we count take away two elements to count the middel
          countMiddle = this.newPost[i].length - 2;
           // we take away the last elements of the string word
          lastElement = this.newPost[i].charAt(this.newPost[i].length - 1);
          this.answer[index]= firstElement + countMiddle + lastElement;
          index++;
     })
     return this.answer;

  
  }
 

}
